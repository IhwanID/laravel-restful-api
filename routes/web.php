<?php

Route::get('/', function () {
    return view('templates.default');
});

Route::get('/stats', function () {
    //$users = App\User::get();
    $stats = [
        'users' => '1000',
        'males' => '1000',
        'females' => '1000',
        'time' => '1000',
        'collection' => '1000',
        'connection' => '1000', 
    ];
    return view('pages.stats', compact('stats'));
});

Route::get('/component', function () {
    
    return view('component');
});




