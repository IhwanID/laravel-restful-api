<div class="right_col" role="main">
<div class="alert alert-danger">
    <h1>{{ $title}} bang {{ $name }}</h1>
    
    {{ $slot }}

</div>
</div>